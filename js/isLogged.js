import { getAuth, onAuthStateChanged } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";

// Add an event listener to check the authentication state
onAuthStateChanged(getAuth(), (user) => {
  if (user) {
    // User is signed in
    // Update the <p> element with the user's email
    const email = user.email;
    const txtEmail = document.querySelector('.txtEmail');

    if (txtEmail) {
      txtEmail.textContent = `${email}`;
    }

    // Update the menu with options for authenticated users as described in the previous answers
    // document.getElementById('menu').innerHTML = '<a href="#">Profile</a><a href="#">Sign Out</a>';
  } else {
    // No user is signed in
    // Update the <p> element to be empty
    const txtEmail = document.querySelector('.txtEmail');

    if (txtEmail) {
      txtEmail.textContent = '';
    }

    // Update the menu with options for unauthenticated users as described in the previous answers
    // document.getElementById('menu').innerHTML = '<a href="#">Sign In</a><a href="#">Sign Up</a>';
  }
});