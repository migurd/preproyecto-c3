import { getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";

// initialize variables
document.addEventListener("DOMContentLoaded", function () {
  const txtEmail = document.querySelector('#txtEmail');
  const txtPassword = document.querySelector('#txtPassword');
  const btnEnviar = document.querySelector('.btnEnviar');

  btnEnviar.addEventListener('click', () => {
    const auth = getAuth();
    signInWithEmailAndPassword(auth, txtEmail.value, txtPassword.value)
      .then((userCredential) => {
        const user = userCredential.user;
        alert(`Signed in as ${user.email}`);
        window.location.href = "/html/menu.html";
      })
      .catch((error) => {
        // const errorMessage = error.message;
        alert(`El usuario o contraseña son inválidos. Intente nuevamente.`);
      });
  });
});