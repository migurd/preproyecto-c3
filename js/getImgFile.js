import { getStorage, ref, uploadBytes } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";

document.addEventListener('DOMContentLoaded', function() {
  const btnPostFile = document.getElementById('btnPostFile');
  const postImage = document.querySelector('.postImage');
  const imageUrlInput = document.querySelector('#menu input[type="text"]');
  const uploadButton = document.querySelector('#menu button');

  // Add an event listener to the file input
  btnPostFile.addEventListener('change', function() {
    const file = this.files[0]; // Get the selected file
    if (file) {
      const fileURL = URL.createObjectURL(file); // Create a URL for the selected file
      postImage.src = fileURL; // Update the image source
      imageUrlInput.value = ''; // Clear the URL input
    }
  });

  // Add an event listener to the URL input
  imageUrlInput.addEventListener('input', function() {
    const imageUrl = this.value.trim();
    if (imageUrl) {
      postImage.src = imageUrl; // Update the image source with the URL
    }
  });

  // Add an event listener to the upload button (you can replace this with your upload logic)
  // Add an event listener to the upload button
  uploadButton.addEventListener('click', function() {
    const selectedFile = btnPostFile.files[0];
    const imageUrl = imageUrlInput.value.trim();

    if (selectedFile) {
      const fileName = selectedFile.name;
      // const fileRef = storageRef.child(fileName);
      const storage = getStorage();
      const storageRef = ref(storage, fileName);

      uploadBytes(storageRef, selectedFile)
        .then((snapshot) => {
          alert(`Se subió el archivo ${fileName}`);
        })
        .catch((error) => {
          alert(`No se pudo subir el archivo: ${fileName}. El error fue ${error}`);
        });
    } else if (imageUrl) {
      // You can also handle URL upload logic here
      alert('Subiendo la imagen desde la URL a Firebase:', imageUrl);
    } else {
      // Handle no selection or invalid input
      alert('No se eligió un archivo.');
    }
  });
  
});
